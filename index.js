#!/usr/bin/env node

const args = process.argv.slice(2);
const cmd = args[0];
const opts = args.slice(1);

switch (cmd) {
  case 'init':
    require('./lib/api/init')(opts);
    break;
  case 'run':
    require('./lib/api/run')(opts);
    break;
  case 'help':
    require('./lib/api/help')(opts);
    break;
  default:
    throw Error(`Invalid command: "hookme ${cmd}" -- Please try again.`);
}


