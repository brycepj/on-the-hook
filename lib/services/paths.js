const globalPaths = global.paths = {};

function setGlobalPath(key, val) {
  return globalPaths[key] = val;
}

function setGlobal(key, val) {
  return global[key] = val;
}

module.exports = {
  setGlobalPath,
  setGlobal
};
