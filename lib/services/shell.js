const shell = require('shelljs');

function executeCommand(command, opts) {
  return new Promise((res, rej) => {
    exec(command, opts, (code, stdout, stderr) => {
      if (code !== 0) reject(stderr);
      resolve(stdout);
    });
  });
}

function executeCommands(commands_arr, opts) {
  const promises = [];

  commands_arr.forEach((command) => {
    promises.push(executeCommand(command, opts));
  });

  return Promise.all(promises);
}

module.exports = {
  executeCommand,
  executeCommands
};
