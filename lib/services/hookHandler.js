function postCommit(args) {
  console.log("Post commit script executed.", args);
}

module.exports = {
  postCommit
};
