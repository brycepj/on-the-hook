const path = require('path');
const fs = require('fs-extra');
const db_dir = require('./paths').db_dir;

function getDb(db_name) {
  const db_file_path = path.join(db_dir, db_name, '.json');
  return new Promise((resolve, reject) => {
    fs.readJson(db_file_path, (err, json) => {
      if (err) reject(err)
      resolve(json);
    });
  });
}

function getDbValue(db_name, key) {
  return getDb(db_name)
    .then((db) => db[key])
    .catch((err) => {
      console.error(`Error occurred while getting Db value: ${db_name} ${key}`);
      console.error(`Stack trace: ${err}`);
    });
}

function getProjectValue(key) {
  return getDbValue('projects', key)
}

function getHookmeValue(key) {
  return getDbValue('projects', key);
}

function setDb(db_name, key, val) {
  const db_file_path = path.join(db_dir, db_name, '.json');
  return getDb(db_name).then((db) => {
    db[key] = val;
    return db;
  }).then(writeUpdatedDb);
}

function writeUpdatedDb(db_path, new_db) {
  return fs.writeJson(db_path, new_db)
    .catch((err) => {
      console.error(`Error occurred while writing to DB: ${db_path}`);
      console.error(`Stack trace: ${err}`);
    });
}
// THIS IS WIP
module.exports = {
  getProjectValue,
  getHookmeValue,
  setProjectValue,
  setHookmeValue
};
