#!/usr/bin/env node

// Parse commands here.
//
// Current options: init (--system), run


// TODO: Interface cleanup
/*
 * Still needed: gem_deviser
 *
 * User flow:
* cd hookme
* rename precommit.sample to precommit
* symlink precommit to index.js
* generate .hookrc
* init branch name (need an API cmd)
* add file to test (need an API cmd)
*
* Configure tests, test runners
* */

const fs = require ('fs');
const path = require('path');
const shell = require('shelljs/global');

const WORKING_DIR = process.cwd();
const HOOKRC_FILEPATH = path.join(WORKING_DIR, '.hookrc');

const files_modified_cmd = "git diff-index --cached --name-only HEAD";

exec(files_modified_cmd, {silent: true}, (code, stdout, stderr) => {
  const files_to_test = fs.readFileSync(HOOKRC_FILEPATH).toString()
    .split('\n')
    .filter((filepath) => filepath !== '')
    .map((filepath) => {
      const gem_name = 'rspec';
      return `bundle exec ${gem_name} ${filepath}`;
    });

  const js_was_modified = stdout.split('\n').filter((changed_path) => {
    return changed_path.endsWith('js') || changed_path.endsWith('es6');
  }).length;

  if (js_was_modified) {
    files_to_test.push('npm run eslint');
  }

  const str_to_exec = `-d ${WORKING_DIR} "${files_to_test.reverse().join('; ')}"`;

  exec(`ttab -w ${str_to_exec}`, (code) => {
    exit(code);
  });
});


// Parse files changed for relevant references??
// Look for registry of files to parse


